<?php

namespace Drupal\payment_saferpay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_saferpay\Service\SaferpayClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for the Callback methods.
 */
class CallbackController extends ControllerBase {

  /**
   * The Saferpay client.
   *
   * @var \Drupal\payment_saferpay\Service\SaferpayClientInterface
   */
  protected $saferpayClient;

  /**
   * The payment status manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('payment_saferpay.client'),
      $container->get('plugin.manager.payment.status')
    );
  }

  /**
   * CallbackController constructor.
   *
   * @param \Drupal\payment_saferpay\Service\SaferpayClientInterface $saferpay_client
   *   The Saferpay client.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   */
  public function __construct(SaferpayClientInterface $saferpay_client, PaymentStatusManagerInterface $payment_status_manager) {
    $this->saferpayClient = $saferpay_client;
    $this->paymentStatusManager = $payment_status_manager;
  }

  /**
   * Abort payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to cancel.
   * @param bool $notify
   *   Notify the user of the success/fail.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Client redirection.
   */
  public function abort(PaymentInterface $payment, bool $notify = TRUE): RedirectResponse {
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_pending') && !$payment->getPaymentStatus()->isOrHasAncestor('payment_authorized')) {
      $this->notify($this->t('An error occured during the payment cancellation: @message.', ['@message' => $this->t('wrong state')]), MessengerInterface::TYPE_WARNING, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    $data = $this->saferpayClient->assert($payment);
    if (FALSE !== $data) {
      $this->notify($this->t('An error occured during the payment cancellation: @message.', ['@message' => $this->t('request failed')]), MessengerInterface::TYPE_ERROR, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    try {
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_cancelled'))
        ->save();

      $this->notify($this->t('Payment has been cancelled.'), MessengerInterface::TYPE_WARNING, $notify);
    }
    catch (\Exception $exception) {
      $this->notify($this->t('An error occured during the payment cancellation: @message.', ['@message' => $this->t('update failed')]), MessengerInterface::TYPE_ERROR, $notify);
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Cancellation failed:' . $exception->getMessage());
    }

    return new RedirectResponse(
      $this->getRedirectUrl($payment)
    );
  }

  /**
   * Update failed payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Failed payment.
   * @param bool $notify
   *   Notify the user of the success/fail.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Client redirection.
   */
  public function fail(PaymentInterface $payment, bool $notify = TRUE): RedirectResponse {
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_pending')) {
      $this->notify($this->t('An error occured during the payment failure: @message.', ['@message' => $this->t('wrong state')]), MessengerInterface::TYPE_WARNING, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    $data = $this->saferpayClient->assert($payment);
    if (FALSE !== $data) {
      $this->notify($this->t('An error occured during the payment failure: @message.', ['@message' => $this->t('request failed')]), MessengerInterface::TYPE_ERROR, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    try {
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance('payment_failed'))
        ->save();

      $this->notify($this->t('Payment failed.'), MessengerInterface::TYPE_WARNING, $notify);
    }
    catch (\Exception $exception) {
      $this->notify($this->t('An error occured during the payment failure: @message.', ['@message' => $this->t('update failed')]), MessengerInterface::TYPE_ERROR, $notify);
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Failure recording failed:' . $exception->getMessage());
    }

    return new RedirectResponse(
      $this->getRedirectUrl($payment)
    );
  }

  /**
   * Confirm payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Confirmed payment.
   * @param bool $notify
   *   Notify the user of the success/fail.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Client redirection.
   */
  public function success(PaymentInterface $payment, bool $notify = TRUE): RedirectResponse {
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_pending')) {
      $this->notify($this->t('An error occured during the payment confirmation: @message.', ['@message' => $this->t('wrong state')]), MessengerInterface::TYPE_WARNING, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    $data = $this->saferpayClient->assert($payment);
    if (empty($data['Transaction']['Status'])) {
      $this->notify($this->t('An error occured during the payment confirmation: @message.', ['@message' => $this->t('request failed')]), MessengerInterface::TYPE_ERROR, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();

    switch ($data['Transaction']['Status']) {
      case SaferpayClientInterface::TRANSACTION_AUTHORIZED:
        $paymentStatus = 'payment_authorized';
        break;

      case SaferpayClientInterface::TRANSACTION_CAPTURED:
        $paymentStatus = $pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success';
        break;

      default:
        $this->notify($this->t('An error occured during the payment confirmation: @message.', ['@message' => $this->t('wrong status')]), MessengerInterface::TYPE_ERROR, $notify);

        return new RedirectResponse(
          $this->getRedirectUrl($payment)
        );
    }

    try {
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance($paymentStatus))
        ->save();
      $this->notify($this->t('Your payment was completed successfully.'), MessengerInterface::TYPE_STATUS, $notify);
    }
    catch (\Exception $exception) {
      $this->notify($this->t('An error occured during the payment confirmation: @message.', ['@message' => $this->t('update failed')]), MessengerInterface::TYPE_ERROR, $notify);
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Confirmation failed:' . $exception->getMessage());

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    if (SaferpayClientInterface::TRANSACTION_AUTHORIZED === $data['Transaction']['Status'] && !empty($pluginDefinition['workflow']['auto_capture'])) {
      $this->capture($payment, FALSE);
    }

    return new RedirectResponse(
      $this->getRedirectUrl($payment)
    );
  }

  /**
   * Capture an authorized payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Confirmed payment.
   * @param bool $notify
   *   Notify the user of the success/fail.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Client redirection.
   */
  public function capture(PaymentInterface $payment, bool $notify = TRUE): RedirectResponse {
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_authorized')) {
      $this->notify($this->t('An error occured during the payment capture: @message.', ['@message' => $this->t('wrong state')]), MessengerInterface::TYPE_WARNING, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    $data = $this->saferpayClient->capture($payment);

    if (empty($data['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $data['Status']) {
      $this->notify($this->t('An error occured during the payment capture: @message.', ['@message' => $this->t('request failed')]), MessengerInterface::TYPE_ERROR, $notify);

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    try {
      $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance($pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success'))
        ->save();
      $this->notify($this->t('Your payment was completed successfully.'), MessengerInterface::TYPE_STATUS, $notify);
    }
    catch (\Exception $exception) {
      $this->notify($this->t('An error occured during the payment capture: @message.', ['@message' => $this->t('update failed')]), MessengerInterface::TYPE_ERROR, $notify);
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Capture failed:' . $exception->getMessage());

      return new RedirectResponse(
        $this->getRedirectUrl($payment)
      );
    }

    return new RedirectResponse(
      $this->getRedirectUrl($payment)
    );
  }

  /**
   * Process Saferpay asynchronous notifications.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Confirmed payment.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Empty response.
   */
  public function update(PaymentInterface $payment): Response {
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_pending') && !$payment->getPaymentStatus()->isOrHasAncestor('payment_authorized')) {
      return new Response('', Response::HTTP_BAD_REQUEST);
    }

    $assertData = $this->saferpayClient->assert($payment);
    if (empty($assertData['Transaction']['Status'])) {
      return new Response('', Response::HTTP_BAD_GATEWAY);
    }

    $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();

    switch ($assertData['Transaction']['Status']) {
      case SaferpayClientInterface::TRANSACTION_AUTHORIZED:
        $paymentStatus = 'payment_authorized';
        break;

      case SaferpayClientInterface::TRANSACTION_CAPTURED:
        $paymentStatus = $pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success';
        break;

      default:
        return new Response('', Response::HTTP_BAD_GATEWAY);
    }

    try {
      $payment->setPaymentStatus($this->paymentStatusManager->createInstance($paymentStatus))
        ->save();
    }
    catch (\Exception $exception) {
      return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    // Auto capture.
    if (SaferpayClientInterface::TRANSACTION_AUTHORIZED === $assertData['Transaction']['Status'] && !empty($pluginDefinition['workflow']['auto_capture'])) {
      $captureData = $this->saferpayClient->capture($payment);

      if (empty($captureData['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $captureData['Status']) {
        return new Response('', Response::HTTP_BAD_GATEWAY);
      }

      try {
        $payment->setPaymentStatus($this->paymentStatusManager->createInstance($pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success'))
          ->save();
      }
      catch (\Exception $exception) {
        return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
      }

    }

    return new Response('', Response::HTTP_OK);
  }

  /**
   * Get redirect URL.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Redirect URL.
   */
  protected function getRedirectUrl(PaymentInterface $payment) {
    return $payment->getPaymentType()
      ->getResumeContextResponse()
      ->getRedirectUrl()
      ->toString(TRUE)
      ->getGeneratedUrl();
  }

  /**
   * Helper function to print messages to the user.
   *
   * @param string $message
   *   Message to print.
   * @param string $type
   *   Message type.
   * @param bool $notify
   *   Whether to print it or not (helper).
   */
  protected function notify(string $message, $type = MessengerInterface::TYPE_STATUS, bool $notify = FALSE) : void {
    if (!$notify) {
      return;
    }

    $this->messenger()
      ->addMessage($message, $type);
  }

}
