<?php

namespace Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationBase;
use Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface;
use Drupal\plugin\PluginType\PluginTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the configuration for the Saferpay PaymentForm.
 *
 * @PaymentMethodConfiguration(
 *   id = "payment_saferpay",
 *   label = @Translation("Saferpay"),
 *   description = @Translation("Redirect the client to the external platform Saferpay (http://www.saferpay.com).")
 * )
 */
class Saferpay extends PaymentMethodConfigurationBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration object containing the Saferpay dedicated settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The payment status plugin type.
   *
   * @var \Drupal\plugin\PluginType\PluginTypeInterface
   */
  protected $paymentStatusType;

  /**
   * The plugin selector manager.
   *
   * @var \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface
   */
  protected $pluginSelectorManager;

  /**
   * SaferpayPaymentFormConfiguration constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   A string containing the English string to translate.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Interface for classes that manage a set of enabled modules.
   * @param \Drupal\plugin\PluginType\PluginTypeInterface $payment_status_type
   *   The payment status plugin type.
   * @param \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorManagerInterface $plugin_selector_manager
   *   The plugin selector manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, TranslationInterface $string_translation, ModuleHandlerInterface $module_handler, PluginTypeInterface $payment_status_type, PluginSelectorManagerInterface $plugin_selector_manager, ConfigFactoryInterface $config_factory) {
    $configuration += $this->defaultConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition, $string_translation, $module_handler);
    $this->paymentStatusType = $payment_status_type;
    $this->pluginSelectorManager = $plugin_selector_manager;
    $this->config = $config_factory->get('payment_saferpay.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('module_handler'),
      $container->get('plugin.plugin_type_manager')->getPluginType('payment_status'),
      $container->get('plugin.manager.plugin.plugin_selector'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return parent::defaultConfiguration() + [
      'account' => [
        'customer_id' => 0,
        'terminal_id' => 0,
      ],
      'auth' => [
        'type' => 'ssl',
        'username' => '',
        'password' => '',
        'test' => TRUE,
      ],
      'workflow' => [
        'auto_capture' => TRUE,
        'notify_url' => TRUE,
        'status_capture_id' => 'payment_success',
        'status_refund_id' => 'payment_refunded',
      ],
      'payment_methods' => [],
      'wallets' => [],
    ];
  }

  /**
   * Sets the customer unique identifier.
   *
   * @param int $customer_id
   *   Customer unique identifier.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setCustomerId(int $customer_id) {
    $this->configuration['account']['customer_id'] = $customer_id;

    return $this;
  }

  /**
   * Gets the customer unique identifier.
   *
   * @return int
   *   The customer unique identifier.
   */
  public function getCustomerId(): int {
    return $this->configuration['account']['customer_id'];
  }

  /**
   * Sets the terminal unique identifier.
   *
   * @param int $terminal_id
   *   Terminal unique identifier.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setTerminalId(int $terminal_id) {
    $this->configuration['account']['terminal_id'] = $terminal_id;

    return $this;
  }

  /**
   * Gets the terminal unique identifier.
   *
   * @return int
   *   Terminal unique identifier.
   */
  public function getTerminalId(): int {
    return $this->configuration['account']['terminal_id'];
  }

  /**
   * Sets the test status.
   *
   * @param bool $test
   *   Test status.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setAuthTest(bool $test) {
    $this->configuration['auth']['test'] = $test;

    return $this;
  }

  /**
   * Gets the test status.
   *
   * @return bool
   *   Test status.
   */
  public function getAuthTest(): bool {
    return $this->configuration['auth']['test'];
  }

  /**
   * Sets the Saferpay API authentication type.
   *
   * @param string $type
   *   API authentication type.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setAuthType(string $type) {
    $this->configuration['auth']['type'] = $type;

    return $this;
  }

  /**
   * Gets the Saferpay API authentication type.
   *
   * @return string
   *   The API authentication type.
   */
  public function getAuthType(): string {
    return $this->configuration['auth']['type'];
  }

  /**
   * Sets the Saferpay API authentication username.
   *
   * @param string $username
   *   API authentication username.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setAuthUsername(string $username) {
    $this->configuration['auth']['username'] = $username;

    return $this;
  }

  /**
   * Gets the Saferpay API authentication username.
   *
   * @return string
   *   The API authentication username.
   */
  public function getAuthUsername(): string {
    return $this->configuration['auth']['username'];
  }

  /**
   * Sets the Saferpay API password.
   *
   * @param string $password
   *   API authentication password.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setAuthPassword(string $password) {
    $this->configuration['auth']['password'] = $password;

    return $this;
  }

  /**
   * Gets the Saferpay API authentication password.
   *
   * @return string
   *   The API authentication password.
   */
  public function getAuthPassword(): string {
    return $this->configuration['auth']['password'];
  }

  /**
   * Sets the auto capture option value.
   *
   * @param bool $auto_capture
   *   Auto capture option value.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setWorkflowAutoCapture(bool $auto_capture) {
    $this->configuration['workflow']['auto_capture'] = $auto_capture;

    return $this;
  }

  /**
   * Gets the auto capture option value.
   *
   * @return bool
   *   Auto capture option value.
   */
  public function getWorkflowAutoCapture(): bool {
    return $this->configuration['workflow']['auto_capture'];
  }

  /**
   * Sets the notify URL option value.
   *
   * @param bool $notify_url
   *   Notify URL option value.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setWorkflowNotifyUrl(bool $notify_url) {
    $this->configuration['workflow']['notify_url'] = $notify_url;

    return $this;
  }

  /**
   * Gets the notify URL option value.
   *
   * @return bool
   *   Notify URL option value.
   */
  public function getWorkflowNotifyUrl(): bool {
    return $this->configuration['workflow']['notify_url'];
  }

  /**
   * Sets the target capture status.
   *
   * @param string $status_capture_id
   *   The plugin ID of the payment status to set.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setWorkflowCaptureStatusId(string $status_capture_id) {
    $this->configuration['workflow']['status_capture_id'] = $status_capture_id;

    return $this;
  }

  /**
   * Gets the target capture status.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getWorkflowCaptureStatusId(): string {
    return $this->configuration['workflow']['status_capture_id'];
  }

  /**
   * Sets the target refund status.
   *
   * @param string $status_refund_id
   *   The plugin ID of the payment status to set.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setWorkflowRefundStatusId(string $status_refund_id) {
    $this->configuration['workflow']['status_refund_id'] = $status_refund_id;

    return $this;
  }

  /**
   * Gets the target refund status.
   *
   * @return string
   *   The plugin ID of the payment status to set.
   */
  public function getWorkflowRefundStatusId() {
    return $this->configuration['workflow']['status_refund_id'];
  }

  /**
   * Sets enabled payment methods.
   *
   * @param array $payment_methods
   *   Payment methods value.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setPaymentMethods(array $payment_methods) {
    $this->configuration['payment_methods'] = array_filter($payment_methods);

    return $this;
  }

  /**
   * Gets enabled payment methods.
   *
   * @return array
   *   The list of enabled payment methods.
   */
  public function getPaymentMethods(): array {
    return $this->configuration['payment_methods'];
  }

  /**
   * Sets enabled wallets.
   *
   * @param array $wallets
   *   Wallets value.
   *
   * @return \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay
   *   The configuration object.
   */
  public function setWallets(array $wallets) {
    $this->configuration['wallets'] = array_filter($wallets);

    return $this;
  }

  /**
   * Gets enabled wallets.
   *
   * @return array
   *   The list of enabled wallets.
   */
  public function getWallets(): array {
    return $this->configuration['wallets'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['account'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Account'),
    ];

    $form['account']['customer_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Customer ID'),
      '#description' => $this->t('Account customer unique identifier'),
      '#min' => 0,
      '#default_value' => $this->getCustomerId(),
      '#required' => TRUE,
    ];

    $form['account']['terminal_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Terminal ID'),
      '#description' => $this->t('Terminal unique identifier'),
      '#min' => 0,
      '#default_value' => $this->getTerminalId(),
      '#required' => TRUE,
    ];

    $form['auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication'),
    ];

    $form['auth']['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Authentication type'),
      '#options' => [
        'ssl' => $this->t('Certificate'),
        'basic' => $this->t('Basic authentication'),
      ],
      '#default_value' => $this->getAuthType(),
      '#required' => TRUE,
    ];

    $form['auth']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('Basic authentication username'),
      '#default_value' => $this->getAuthUsername(),
      '#states' => [
        'visible' => [
          ':input[name="plugin_form[auth][type]"]' => ['value' => 'basic'],
        ],
        'required' => [
          ':input[name="plugin_form[auth][type]"]' => ['value' => 'basic'],
        ],
      ],
    ];

    $form['auth']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Basic authentication password'),
      '#default_value' => $this->getAuthPassword(),
      '#states' => [
        'visible' => [
          ':input[name="plugin_form[auth][type]"]' => ['value' => 'basic'],
        ],
        'required' => [
          ':input[name="plugin_form[auth][type]"]' => ['value' => 'basic'],
        ],
      ],
    ];

    $form['auth']['test'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Test mode'),
      '#description' => $this->t('Request will use the Saferpay test API, no money will be transferred.'),
      '#default_value' => $this->getAuthTest(),
    ];

    $form['workflow'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Workflow'),
    ];

    $form['workflow']['auto_capture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Capture automatically'),
      '#description' => $this->t('Try to capture automatically authorized payments.'),
      '#default_value' => $this->getWorkflowAutoCapture(),
    ];

    $form['workflow']['notify_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Saferpay asynchronous updates'),
      '#description' => $this->t('Allow Saferpay to update payments asynchronously, recommended but requires the website to be accessible from Saferpay.'),
      '#default_value' => $this->getWorkflowNotifyUrl(),
    ];

    $form['workflow']['status_capture_id'] = $this->getCapturePaymentStatusSelector($form_state)
      ->buildSelectorForm([], $form_state);

    $form['workflow']['status_refund_id'] = $this->getRefundPaymentStatusSelector($form_state)
      ->buildSelectorForm([], $form_state);

    $form['payment_methods'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Payment methods'),
      '#description' => '<p>' . $this->t('The standard payment methods and wallets actually provided to the end user depend on the terms of your contract.')
      . '<br />' . $this->t('Uncheck all standard payment methods and all wallets if you want Saferpay to determine which ones will be provided to the end user.') . '</p>',
    ];

    $form['payment_methods']['payment_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Standard payment methods'),
      '#options' => $this->getPaymentMethodsOptionsList(),
      '#default_value' => $this->getPaymentMethods(),
    ];

    $form['payment_methods']['wallets'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Wallets'),
      '#options' => $this->getWalletsOptionsList(),
      '#default_value' => $this->getWallets(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $this->getCapturePaymentStatusSelector($form_state)->validateSelectorForm($form['workflow']['status_capture_id'], $form_state);
    $this->getRefundPaymentStatusSelector($form_state)->validateSelectorForm($form['workflow']['status_refund_id'], $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->getCapturePaymentStatusSelector($form_state)->submitSelectorForm($form['workflow']['status_capture_id'], $form_state);
    $this->getRefundPaymentStatusSelector($form_state)->submitSelectorForm($form['workflow']['status_refund_id'], $form_state);

    $values = $form_state->getValue('plugin_form');

    $this->setCustomerId((int) $values['account']['customer_id'])
      ->setTerminalId((int) $values['account']['terminal_id'])
      ->setAuthType($values['auth']['type'])
      ->setAuthUsername($values['auth']['username'])
      ->setAuthPassword($values['auth']['password'])
      ->setAuthTest((bool) $values['auth']['test'])
      ->setWorkflowAutoCapture((bool) $values['workflow']['auto_capture'])
      ->setWorkflowNotifyurl((bool) $values['workflow']['notify_url'])
      ->setWorkflowCaptureStatusId(
        $this->getCapturePaymentStatusSelector($form_state)
          ->getSelectedPlugin()
          ->getPluginId()
      )
      ->setWorkflowRefundStatusId(
        $this->getRefundPaymentStatusSelector($form_state)
          ->getSelectedPlugin()
          ->getPluginId()
      )
      ->setPaymentMethods($values['payment_methods']['payment_methods'])
      ->setWallets($values['payment_methods']['wallets']);
  }

  /**
   * Gets the payment status selector for the capture phase.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorInterface
   *   Payment statuses plugin selector.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getCapturePaymentStatusSelector(FormStateInterface $form_state) {
    return $this->getPaymentStatusSelector($form_state, 'capture', $this->getWorkflowCaptureStatusId())
      ->setLabel($this->t('Capture status'))
      ->setDescription($this->t('The status to set payments to after being captured by this payment method.'));
  }

  /**
   * Gets the payment status selector for the refund phase.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorInterface
   *   Payment statuses plugin selector.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getRefundPaymentStatusSelector(FormStateInterface $form_state) {
    return $this->getPaymentStatusSelector($form_state, 'refund', $this->getWorkflowRefundStatusId())
      ->setLabel($this->t('Refund status'))
      ->setDescription($this->t('The status to set payments to after being refunded by this payment method.'));
  }

  /**
   * Gets the payment status selector.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param string $type
   *   Payment step.
   * @param string $default_plugin_id
   *   Default payment status.
   *
   * @return \Drupal\plugin\Plugin\Plugin\PluginSelector\PluginSelectorInterface|mixed
   *   Payment statuses plugin selector.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getPaymentStatusSelector(FormStateInterface $form_state, string $type, string $default_plugin_id) {
    $key = 'payment_status_selector_' . $type;

    if ($form_state->has($key)) {
      return $form_state->get($key);
    }

    $plugin_selector = $this->pluginSelectorManager->createInstance('payment_select_list');
    $plugin_selector->setSelectablePluginType($this->paymentStatusType);
    $plugin_selector->setRequired(TRUE);
    $plugin_selector->setCollectPluginConfiguration(FALSE);
    $plugin_selector->setSelectedPlugin(
      $this->paymentStatusType->getPluginManager()
        ->createInstance($default_plugin_id)
    );

    $form_state->set($key, $plugin_selector);

    return $plugin_selector;
  }

  /**
   * Get available payment methods as an option list.
   *
   * @return array
   *   The list of available payment methods.
   */
  protected function getPaymentMethodsOptionsList(): array {
    $methods = $this->config->get('payment_methods');
    if (empty($methods)) {
      return [];
    }

    $list = [];
    foreach ($methods as $id => $data) {
      $list[$id] = $data['label'];
    }

    asort($list, SORT_STRING | SORT_FLAG_CASE);
    return $list;
  }

  /**
   * Get available wallets as an option list.
   *
   * @return array
   *   The list of available wallets.
   */
  protected function getWalletsOptionsList(): array {
    $wallets = $this->config->get('wallets');
    if (empty($wallets)) {
      return [];
    }

    $list = [];
    foreach ($wallets as $id => $data) {
      $list[$id] = $data['label'];
    }

    asort($list, SORT_STRING | SORT_FLAG_CASE);
    return $list;
  }

}
