<?php

namespace Drupal\payment_saferpay\Plugin\Payment\Method;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\payment\EventDispatcherInterface;
use Drupal\payment\OperationResult;
use Drupal\payment\Plugin\Payment\Method\PaymentMethodBase;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment\Response\Response;
use Drupal\payment_saferpay\Service\SaferpayClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Saferpay Payment Form payment method.
 *
 * @PaymentMethod(
 *   id = "payment_saferpay",
 *   label = @Translation("Saferpay"),
 *   deriver = "\Drupal\payment_saferpay\Plugin\Payment\Method\SaferpayDeriver"
 * )
 */
class Saferpay extends PaymentMethodBase implements ContainerFactoryPluginInterface, ConfigurableInterface {

  use MessengerTrait;

  /**
   * The Saferpay client.
   *
   * @var \Drupal\payment_saferpay\Service\SaferpayClientInterface
   */
  protected $saferpayClient;

  /**
   * Constructs a new instance.
   *
   * @param mixed[] $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed[] $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\payment\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Utility\Token $token
   *   The token API.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \Drupal\payment_saferpay\Service\SaferpayClientInterface $client
   *   The Saferpay client.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher,
    Token $token,
    PaymentStatusManagerInterface $payment_status_manager,
    SaferpayClientInterface $client
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $event_dispatcher, $token, $payment_status_manager);

    $this->saferpayClient = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('payment.event_dispatcher'),
      $container->get('token'),
      $container->get('plugin.manager.payment.status'),
      $container->get('payment_saferpay.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentExecutionResult() {
    $payment = $this->getPayment();
    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_pending') || NULL !== $this->saferpayClient->getValidToken($payment)) {
      // Avoid processing an ongoing transaction.
      return new OperationResult();
    }

    $data = $this->saferpayClient->initialize($payment);
    if (empty($data['RedirectUrl'])) {
      \Drupal::messenger()->addError($this->t('Saferpay payment initialization failed, please try again later. If the problem persists, please contact the administrator.'));

      return new OperationResult(
        new Response(Url::fromRoute('<current>'))
      );
    }

    return new OperationResult(
      new Response(Url::fromUri($data['RedirectUrl']))
    );
  }

  /**
   * Gets the last known transaction id.
   *
   * @return string|null
   *   The transaction id.
   */
  public function getTransactionId() {
    return isset($this->configuration['transaction_id']) ? $this->configuration['transaction_id'] : NULL;
  }

  /**
   * Sets the current transaction id.
   *
   * @param string $transaction_id
   *   The transaction id.
   */
  public function setTransactionId(string $transaction_id): void {
    $this->configuration['transaction_id'] = $transaction_id;
  }

  /**
   * {@inheritdoc}
   */
  protected function doCapturePaymentAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'payment.payment.capture.any');
  }

  /**
   * {@inheritdoc}
   */
  protected function doCapturePayment() {
    $payment = $this->getPayment();

    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_authorized')) {
      $this->messenger()
        ->addError('Only authorized payments can be captured.');
      return;
    }

    $data = $this->saferpayClient->capture($payment);

    if (empty($data['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $data['Status']) {
      $this->messenger()
        ->addError('Request to Saferpay failed.');
      return;
    }

    try {
      $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();
      $paymentStatus = $this->paymentStatusManager->createInstance($pluginDefinition['workflow']['status_capture_id']);
      $payment->setPaymentStatus($paymentStatus)
        ->save();
    }
    catch (\Exception $exception) {
      $this->messenger()
        ->addError('Saferpay request succeeded but payment entity could not be updated.');
      return;
    }

    $this->messenger()
      ->addStatus('Payment successfully captured.');
  }

  /**
   * {@inheritdoc}
   */
  protected function doRefundPaymentAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'payment.payment.refund.any');
  }

  /**
   * {@inheritdoc}
   */
  protected function doRefundPayment() {
    $payment = $this->getPayment();

    if (!$payment->getPaymentStatus()->isOrHasAncestor('payment_money_transferred')) {
      $this->messenger()
        ->addError('Only captured payments can be refunded.');
      return;
    }

    $data = $this->saferpayClient->refund($payment);

    if (empty($data['Transaction']['Id']) || SaferpayClientInterface::TRANSACTION_AUTHORIZED !== $data['Transaction']['Status']) {
      $this->messenger()
        ->addError('Refund authorization request to Saferpay failed.');
      return;
    }

    try {
      $paymentStatus = $this->paymentStatusManager->createInstance('refund_authorized');
      $payment->setPaymentStatus($paymentStatus)
        ->save();
    }
    catch (\Exception $exception) {
      $this->messenger()
        ->addError('Refund authorization request to Saferpay succeeded but payment entity could not be updated.');
      return;
    }

    // Auto capture.
    $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();
    if (!empty($pluginDefinition['workflow']['auto_capture'])) {
      $successMessage = 'Payment successfully refunded.';
      $captureData = $this->saferpayClient->capture($payment);

      if (empty($captureData['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $captureData['Status']) {
        $this->messenger()
          ->addError('Refund capture request to Saferpay failed.');
        return;
      }

      try {
        $paymentStatus = $this->paymentStatusManager->createInstance($pluginDefinition['workflow']['status_refund_id']);
        $payment->setPaymentStatus($paymentStatus)
          ->save();
      }
      catch (\Exception $exception) {
        $this->messenger()
          ->addError('Refund capture request to Saferpay succeeded but payment entity could not be updated.');
        return;
      }
    }

    $this->messenger()
      ->addStatus($successMessage ?? 'Refund successfully requested.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getSupportedCurrencies() {
    return TRUE;
  }

}
