<?php

namespace Drupal\payment_saferpay\Plugin\Payment\Method;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derives payment method plugin definitions based on configuration entities.
 *
 * @see \Drupal\payment\Plugin\Payment\Method\Basic
 */
class SaferpayDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The payment method configuration manager.
   *
   * @var \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface
   */
  protected $paymentMethodConfigurationManager;

  /**
   * The payment method configuration storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentMethodConfigurationStorage;

  /**
   * SaferpayPaymentFormDeriver constructor.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $payment_method_configuration_storage
   *   The payment method configuration storage.
   * @param \Drupal\payment\Plugin\Payment\MethodConfiguration\PaymentMethodConfigurationManagerInterface $payment_method_configuration_manager
   *   The payment method configuration manager.
   */
  public function __construct(EntityStorageInterface $payment_method_configuration_storage, PaymentMethodConfigurationManagerInterface $payment_method_configuration_manager) {
    $this->paymentMethodConfigurationStorage = $payment_method_configuration_storage;
    $this->paymentMethodConfigurationManager = $payment_method_configuration_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager')
        ->getStorage('payment_method_configuration'),
      $container->get('plugin.manager.payment.method_configuration')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\payment\Entity\PaymentMethodConfigurationInterface[] $payment_methods */
    $payment_methods = $this->paymentMethodConfigurationStorage->loadMultiple();

    foreach ($payment_methods as $payment_method) {
      if ('payment_saferpay' === $payment_method->getPluginId()) {
        /** @var \Drupal\payment_saferpay\Plugin\Payment\MethodConfiguration\Saferpay $configuration_plugin */
        $configuration_plugin = $this->paymentMethodConfigurationManager->createInstance($payment_method->getPluginId(), $payment_method->getPluginConfiguration());

        $this->derivatives[$payment_method->id()] = [
          'id' => $base_plugin_definition['id'] . ':' . $payment_method->id(),
          'active' => $payment_method->status(),
          'label' => $payment_method->label(),
          'message_text' => $configuration_plugin->getMessageText(),
          'message_text_format' => $configuration_plugin->getMessageTextFormat(),
          'account' => [
            'customer_id' => $configuration_plugin->getCustomerId(),
            'terminal_id' => $configuration_plugin->getTerminalId(),
          ],
          'auth' => [
            'type' => $configuration_plugin->getAuthType(),
            'username' => $configuration_plugin->getAuthUsername(),
            'password' => $configuration_plugin->getAuthPassword(),
            'test' => $configuration_plugin->getAuthTest(),
          ],
          'workflow' => [
            'auto_capture' => $configuration_plugin->getWorkflowAutoCapture(),
            'notify_url' => $configuration_plugin->getWorkflowNotifyUrl(),
            'status_capture_id' => $configuration_plugin->getWorkflowCaptureStatusId(),
            'status_refund_id' => $configuration_plugin->getWorkflowRefundStatusId(),
          ],
          'payment_methods' => $configuration_plugin->getPaymentMethods(),
          'wallets' => $configuration_plugin->getWallets(),
        ] + $base_plugin_definition;
      }
    }

    return $this->derivatives;
  }

}
