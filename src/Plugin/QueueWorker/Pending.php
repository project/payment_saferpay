<?php

namespace Drupal\payment_saferpay\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface;
use Drupal\payment_saferpay\Service\SaferpayClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Processes pending Saferpay payments.
 *
 * @QueueWorker(
 *   id = "payment_saferpay_pending",
 *   title = @Translation("Process pending Saferpay payments to fetch status update."),
 *   cron = {"time" = 30}
 * )
 */
class Pending extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;

  /**
   * The payment entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * The payment status manager.
   *
   * @var \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface
   */
  protected $paymentStatusManager;

  /**
   * The Saferpay client.
   *
   * @var \Drupal\payment_saferpay\Service\SaferpayClientInterface
   */
  protected $saferpayClient;

  /**
   * Constructs a SaferpayPending object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\payment\Plugin\Payment\Status\PaymentStatusManagerInterface $payment_status_manager
   *   The payment status manager.
   * @param \Drupal\payment_saferpay\Service\SaferpayClientInterface $saferpay_client
   *   The Saferpay client.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentStatusManagerInterface $payment_status_manager,
    SaferpayClientInterface $saferpay_client
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->paymentStorage = $entity_type_manager->getStorage('payment');
    $this->paymentStatusManager = $payment_status_manager;
    $this->saferpayClient = $saferpay_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.payment.status'),
      $container->get('payment_saferpay.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($payment_id) {
    /** @var \Drupal\payment\Entity\PaymentInterface $payment */
    $payment = $this->paymentStorage->load($payment_id);

    // Ensure the entity is processable.
    if (!$payment
      || !$payment->getPaymentStatus()->isOrHasAncestor('payment_pending')
      || 0 !== strpos($payment->getPaymentMethod()->getPluginId(), 'payment_saferpay:')
      || (time() - 900) < $payment->getCreatedTime()) {
      $this->getLogger('payment_saferpay.pending')
        ->warning('Payment ' . $payment_id . ': Payment is unprocessable.');
      return;
    }

    // Ensure valid token.
    $token = $this->saferpayClient->getValidToken($payment);
    if (empty($token)) {
      $this->getLogger('payment_saferpay.pending')
        ->warning('Payment ' . $payment_id . ': Token is ' . (FALSE === $token ? 'expired' : 'unavailable') . '.');

      // @todo Make a final inquire request before cancelling payment ?
      $this->cancelPayment($payment);
      return;
    }

    // Saferpay request.
    $assertData = $this->saferpayClient->assert($payment);
    if (FALSE === $assertData) {
      $this->getLogger('payment_saferpay.pending')
        ->warning('Payment ' . $payment_id . ': Payment is unknown from Saferpay.');

      $this->cancelPayment($payment);
      return;
    }
    if (empty($assertData['Transaction']['Status'])) {
      $this->getLogger('payment_saferpay.pending')
        ->error('Payment ' . $payment_id . ': Saferpay request failed.');
      return;
    }

    $pluginDefinition = $payment->getPaymentMethod()->getPluginDefinition();

    switch ($assertData['Transaction']['Status']) {
      case SaferpayClientInterface::TRANSACTION_AUTHORIZED:
        $paymentStatusId = 'payment_authorized';
        break;

      case SaferpayClientInterface::TRANSACTION_CAPTURED:
        $paymentStatusId = $pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success';
        break;

      default:
        return new Response('', Response::HTTP_BAD_GATEWAY);
    }

    $paymentStatus = $this->paymentStatusManager->createInstance($paymentStatusId);
    $payment->setPaymentStatus($paymentStatus)
      ->save();

    // Auto capture.
    if (SaferpayClientInterface::TRANSACTION_AUTHORIZED === $assertData['Transaction']['Status'] && !empty($pluginDefinition['workflow']['auto_capture'])) {
      $captureData = $this->saferpayClient->capture($payment);

      if (empty($captureData['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $captureData['Status']) {
        $this->getLogger('payment_saferpay.pending')
          ->error('Payment ' . $payment_id . ': Saferpay auto-capture request failed.');
        return;
      }

      $paymentStatus = $this->paymentStatusManager->createInstance($pluginDefinition['workflow']['status_capture_id'] ?? 'payment_success');
      $payment->setPaymentStatus($paymentStatus)
        ->save();
    }
  }

  /**
   * Cancel payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to cancel.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function cancelPayment(PaymentInterface $payment) {
    $paymentStatus = $this->paymentStatusManager->createInstance('payment_cancelled');

    $payment->setPaymentStatus($paymentStatus)
      ->save();
  }

}
