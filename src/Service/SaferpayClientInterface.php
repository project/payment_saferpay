<?php

namespace Drupal\payment_saferpay\Service;

use Drupal\payment\Entity\PaymentInterface;

/**
 * Interface for Client abstraction to contact Saferpay endpoints.
 */
interface SaferpayClientInterface {

  /**
   * Transaction has been authorized but no money was transferred.
   */
  const TRANSACTION_AUTHORIZED = 'AUTHORIZED';

  /**
   * Transaction is completed.
   */
  const TRANSACTION_CAPTURED = 'CAPTURED';

  /**
   * Transaction is captured.
   */
  const ERR_CAPTURED = 'TRANSACTION_ALREADY_CAPTURED';

  /**
   * Transaction is pending payment.
   */
  const TRANSACTION_PENDING = 'PENDING';

  /**
   * Transaction type is a refund.
   */
  const TRANSACTION_TYPE_REFUND = 'REFUND';

  /**
   * Initialize payment process.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return array|null
   *   Response data or NULL.
   */
  public function initialize(PaymentInterface $payment);

  /**
   * Assert payment status.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return array|bool|null
   *   Response data, FALSE if the payment is unknown or NULL.
   */
  public function assert(PaymentInterface $payment);

  /**
   * Capture an authorized payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return array|bool|null
   *   Response data, FALSE if the payment is unknown or NULL.
   */
  public function capture(PaymentInterface $payment);

  /**
   * Refund a captured payment.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return array|bool|null
   *   Response data, FALSE if the payment is unknown or NULL.
   */
  public function refund(PaymentInterface $payment);

  /**
   * Get valid token for assert requests.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return null|bool|string
   *   Valid token, FALSE if expired or NULL.
   */
  public function getValidToken(PaymentInterface $payment);

}
