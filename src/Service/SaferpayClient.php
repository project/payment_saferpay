<?php

namespace Drupal\payment_saferpay\Service;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\payment\Entity\PaymentInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Client abstraction to contact Saferpay endpoints.
 */
class SaferpayClient implements SaferpayClientInterface {

  use LoggerChannelTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Saferpay configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Current language.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected $language;

  /**
   * The CSRF Token Generator.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * SaferpayClient constructor.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state storage.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Access\CsrfTokenGenerator $csrf_token
   *   The CSRF Token Generator.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, StateInterface $state, LanguageManagerInterface $language_manager, CsrfTokenGenerator $csrf_token, AccountProxyInterface $current_user, RequestStack $request_stack) {
    $this->httpClient = $http_client;
    $this->config = $config_factory->get('payment_saferpay.settings');
    $this->state = $state;
    $this->language = $language_manager->getCurrentLanguage();
    $this->csrfToken = $csrf_token;
    $this->currentUser = $current_user;
    $this->currentRequest = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function initialize(PaymentInterface $payment) {
    $this->validatePlugin($payment);

    if (!($config = $this->buildConfig($payment))) {
      return NULL;
    }

    $this->ensureSession();

    $requestOptions = $this->buildRequestOptions($payment, $config);

    // Format description.
    $descriptions = [];
    foreach ($payment->getLineItems() as $lineItem) {
      $descriptions[] = $lineItem->getDescription();
    }

    // Request body.
    $requestOptions[RequestOptions::JSON]['TerminalId'] = $config['account']['terminal_id'];
    $requestOptions[RequestOptions::JSON]['Payer'] = [
      'LanguageCode' => $this->language->getId(),
    ];
    $requestOptions[RequestOptions::JSON]['Payment'] = [
      'Amount' => [
        'Value' => intval($payment->getAmount() * $payment->getCurrency()->getSubunits()),
        'CurrencyCode' => $payment->getCurrencyCode(),
      ],
      'OrderId' => $payment->id(),
      'Description' => implode(', ', $descriptions),
    ];
    if (!empty($config['payment_methods'])) {
      $requestOptions[RequestOptions::JSON]['PaymentMethods'] = $config['payment_methods'];
    }
    if (!empty($config['wallets'])) {
      $requestOptions[RequestOptions::JSON]['Wallets'] = $config['wallets'];
    }
    $requestOptions[RequestOptions::JSON]['ReturnUrls'] = [
      'Success' => $this->buildSecureUrl($payment, 'payment_saferpay.callback_success')->toString(TRUE)->getGeneratedUrl(),
      'Fail' => $this->buildSecureUrl($payment, 'payment_saferpay.callback_fail')->toString(TRUE)->getGeneratedUrl(),
      'Abort' => $this->buildSecureUrl($payment, 'payment_saferpay.callback_abort')->toString(TRUE)->getGeneratedUrl(),
    ];
    if (!empty($config['workflow']['notify_url'])) {
      $requestOptions[RequestOptions::JSON]['Notification'] = [
        'NotifyUrl' => $this->buildSecureUrl($payment, 'payment_saferpay.callback_update')->toString(TRUE)->getGeneratedUrl(),
      ];
    }

    $response = $this->httpClient
      ->post($this->getApiUrl('page_initialize', $config), $requestOptions);
    if (Response::HTTP_OK !== $response->getStatusCode()) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay initialization failed: ' . $response->getReasonPhrase());
      return NULL;
    }

    $data = json_decode($response->getBody(), TRUE);
    if (!$this->validateInitializeData($payment, $data)) {
      return NULL;
    }

    $this->state->set('payment_saferpay.' . $payment->id() . '.token', [
      'token' => $data['Token'],
      'expiration' => strtotime($data['Expiration']),
    ]);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function assert(PaymentInterface $payment) {
    $this->validatePlugin($payment);

    // Fetch token.
    $token = $this->getValidToken($payment);
    if (!isset($token)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay assert failed: no token available.');
      return NULL;
    }
    elseif (FALSE === $token) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay assert failed: token expired.');
      return NULL;
    }

    if (!($config = $this->buildConfig($payment))) {
      return NULL;
    }

    $requestOptions = $this->buildRequestOptions($payment, $config);

    // Request body.
    $requestOptions[RequestOptions::JSON]['Token'] = $token;

    $response = $this->httpClient
      ->post($this->getApiUrl('page_assert', $config), $requestOptions);

    if (Response::HTTP_PAYMENT_REQUIRED === $response->getStatusCode()) {
      // Transaction is unknown.
      return FALSE;
    }
    elseif (Response::HTTP_OK !== $response->getStatusCode()) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay assert failed: ' . $response->getReasonPhrase());
      return NULL;
    }

    $data = json_decode($response->getBody(), TRUE);
    if (!$this->validateAssertData($payment, $data)) {
      return NULL;
    }

    // Transaction id can be stored to capture the payment.
    $payment->getPaymentMethod()->setTransactionId($data['Transaction']['Id']);
    $payment->save();

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function capture(PaymentInterface $payment) {
    $this->validatePlugin($payment);

    if (!($config = $this->buildConfig($payment))) {
      return NULL;
    }

    $requestOptions = $this->buildRequestOptions($payment, $config);

    // Request body.
    $transaction_id = $payment->getPaymentMethod()->getTransactionId();
    if (!empty($transaction_id)) {
      $requestOptions[RequestOptions::JSON]['TransactionReference']['TransactionId'] = $transaction_id;
    }
    else {
      $requestOptions[RequestOptions::JSON]['TransactionReference']['OrderId'] = $payment->id();
    }
    $requestOptions[RequestOptions::JSON]['Amount'] = [
      'Value' => intval($payment->getAmount() * $payment->getCurrency()->getSubunits()),
      'CurrencyCode' => $payment->getCurrencyCode(),
    ];

    $response = $this->httpClient
      ->post($this->getApiUrl('transaction_capture', $config), $requestOptions);

    if (Response::HTTP_PAYMENT_REQUIRED === $response->getStatusCode()) {
      $data = json_decode($response->getBody(), TRUE);
      // Transaction already captured.
      if (isset($data['ErrorName']) && SaferpayClientInterface::ERR_CAPTURED === $data['ErrorName']) {
        return [
          'Status' => SaferpayClientInterface::TRANSACTION_CAPTURED,
        ];
      }
      // Transaction is unknown.
      return FALSE;
    }
    elseif (Response::HTTP_OK !== $response->getStatusCode()) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay capture failed: ' . $response->getReasonPhrase());
      return NULL;
    }

    $data = json_decode($response->getBody(), TRUE);
    if (!$this->validateCaptureData($payment, $data)) {
      return NULL;
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function refund(PaymentInterface $payment) {
    $this->validatePlugin($payment);

    if (!($config = $this->buildConfig($payment))) {
      return NULL;
    }

    $requestOptions = $this->buildRequestOptions($payment, $config);

    // Request body.
    $requestOptions[RequestOptions::JSON]['CaptureReference']['OrderId'] = $payment->id();
    $requestOptions[RequestOptions::JSON]['Refund']['Amount'] = [
      'Value' => intval($payment->getAmount() * $payment->getCurrency()->getSubunits()),
      'CurrencyCode' => $payment->getCurrencyCode(),
    ];

    $response = $this->httpClient
      ->post($this->getApiUrl('transaction_refund', $config), $requestOptions);

    if (Response::HTTP_PAYMENT_REQUIRED === $response->getStatusCode()) {
      // Transaction is unknown.
      return FALSE;
    }
    elseif (Response::HTTP_OK !== $response->getStatusCode()) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Saferpay refund failed: ' . $response->getReasonPhrase());
      return NULL;
    }

    $data = json_decode($response->getBody(), TRUE);
    if (!$this->validateRefundData($payment, $data)) {
      return NULL;
    }

    // Transaction id is necessary to capture the refund.
    $payment->getPaymentMethod()->setTransactionId($data['Transaction']['Id']);
    $payment->save();

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidToken(PaymentInterface $payment) {
    $tokenState = $this->state->get('payment_saferpay.' . $payment->id() . '.token');

    if (empty($tokenState['token'])) {
      return NULL;
    }
    elseif (time() >= $tokenState['expiration']) {
      return FALSE;
    }

    return $tokenState['token'];
  }

  /**
   * Validate configuration.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   *
   * @return array|bool
   *   Built config or FALSE.
   */
  protected function buildConfig(PaymentInterface $payment) {
    $errors = [];

    $plugin_definition = $payment->getPaymentMethod()->getPluginDefinition();
    $config = [
      'auth' => $plugin_definition['auth'],
      'account' => $plugin_definition['account'],
      'workflow' => $plugin_definition['workflow'],
      'payment_methods' => array_values($plugin_definition['payment_methods']),
      'wallets' => array_values($plugin_definition['wallets']),
    ];

    if (empty($config['auth']['type'])) {
      $errors[] = 'config must contain "auth".';
    }
    if ('basic' === $config['auth']['type'] && empty($config['auth']['username'])) {
      $errors[] = 'basic authentication requires "auth.username".';
    }
    if ('basic' === $config['auth']['type'] && empty($config['auth']['password'])) {
      $errors[] = 'basic authentication requires "auth.password".';
    }

    if (empty($config['account']['customer_id'])) {
      $errors[] = 'config must contain "account.customer_id".';
    }
    if (empty($config['account']['terminal_id'])) {
      $errors[] = 'config must contain "account.terminal_id".';
    }

    if (!empty($errors)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Config validation failed: ' . implode(', ', $errors));
      return FALSE;
    }

    return $config;
  }

  /**
   * Build request options.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param array $config
   *   Plugin configuration.
   *
   * @return array
   *   Request options.
   */
  protected function buildRequestOptions(PaymentInterface $payment, array $config): array {
    $requestOptions = [
      'http_errors' => FALSE,
    ];

    // Authentication.
    if ('basic' === $config['auth']['type']) {
      $requestOptions[RequestOptions::AUTH] = [
        $config['auth']['username'],
        $config['auth']['password'],
      ];
    }

    $requestOptions[RequestOptions::JSON] = [
      'RequestHeader' => [
        'SpecVersion' => '1.13',
        'CustomerId' => $config['account']['customer_id'],
        'RequestId' => $payment->id() . '-' . time(),
        'RetryIndicator' => 0,
      ],
    ];

    return $requestOptions;
  }

  /**
   * Build secure URL from route name.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param string $route_name
   *   Route name to access.
   *
   * @return \Drupal\Core\Url
   *   Generated URL with CSRF token.
   */
  protected function buildSecureUrl(PaymentInterface $payment, string $route_name): Url {
    $url = Url::fromRoute($route_name, ['payment' => $payment->id()], ['absolute' => TRUE]);

    $token = $this->csrfToken->get($url->getInternalPath());
    $url->setOption('query', ['token' => $token]);

    return $url;
  }

  /**
   * Validate plugin.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   */
  protected function validatePlugin(PaymentInterface $payment):void {
    $plugin_ids = explode(':', $payment->getPaymentMethod()->getPluginId());

    if ('payment_saferpay' !== $plugin_ids[0]) {
      throw new \LogicException('SaferpayClient only supports "payment_saferpay" method, "' . $plugin_ids[0] . '" given.');
    }
  }

  /**
   * Validate initialize data.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param array $data
   *   Data to validate.
   *
   * @return bool
   *   Whether the data is valid.
   */
  protected function validateInitializeData(PaymentInterface $payment, array $data): bool {
    $errors = [];

    if (empty($data['ResponseHeader']['RequestId']) || 0 !== strpos($data['ResponseHeader']['RequestId'], $payment->id() . '-')) {
      $errors[] = 'data must contain valid "ResponseHeader.RequestId".';
    }
    if (empty($data['Token'])) {
      $errors[] = 'data must contain "Token".';
    }
    if (empty($data['Expiration'])) {
      $errors[] = 'data must contain valid "Expiration".';
    }
    if (empty($data['RedirectUrl']) && !UrlHelper::isValid($data['RedirectUrl'])) {
      $errors[] = 'data must contain valid "RedirectUrl".';
    }

    if (!empty($errors)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Initialize data validation failed: ' . implode(', ', $errors));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate assert data.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param array $data
   *   Data to validate.
   *
   * @return bool
   *   Whether the data is valid.
   */
  protected function validateAssertData(PaymentInterface $payment, array $data): bool {
    $errors = [];

    if (empty($data['ResponseHeader']['RequestId']) || 0 !== strpos($data['ResponseHeader']['RequestId'], $payment->id() . '-')) {
      $errors[] = 'data must contain valid "ResponseHeader.RequestId".';
    }
    if (empty($data['Transaction']['Id'])) {
      $errors[] = 'data must contain "Transaction.Id".';
    }
    if (empty($data['Transaction']['Status'])) {
      $errors[] = 'data must contain "Transaction.Status".';
    }
    if (empty($data['Transaction']['Amount']['Value'])) {
      $errors[] = 'data must contain "Transaction.Amount.Value".';
    }
    if (empty($data['Transaction']['Amount']['CurrencyCode'])) {
      $errors[] = 'data must contain "Transaction.Amount.CurrencyCode".';
    }

    if (!empty($errors)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Assert data validation failed: ' . implode(', ', $errors));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate capture data.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param array $data
   *   Data to validate.
   *
   * @return bool
   *   Whether the data is valid.
   */
  protected function validateCaptureData(PaymentInterface $payment, array $data): bool {
    $errors = [];

    if (empty($data['ResponseHeader']['RequestId']) || 0 !== strpos($data['ResponseHeader']['RequestId'], $payment->id() . '-')) {
      $errors[] = 'data must contain valid "ResponseHeader.RequestId".';
    }
    if (empty($data['Status']) || SaferpayClientInterface::TRANSACTION_CAPTURED !== $data['Status']) {
      $errors[] = 'data must contain valid "Status".';
    }

    if (!empty($errors)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Capture data validation failed: ' . implode(', ', $errors));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Validate refund data.
   *
   * @param \Drupal\payment\Entity\PaymentInterface $payment
   *   Payment to process.
   * @param array $data
   *   Data to validate.
   *
   * @return bool
   *   Whether the data is valid.
   */
  protected function validateRefundData(PaymentInterface $payment, array $data): bool {
    $errors = [];

    if (empty($data['ResponseHeader']['RequestId']) || 0 !== strpos($data['ResponseHeader']['RequestId'], $payment->id() . '-')) {
      $errors[] = 'data must contain valid "ResponseHeader.RequestId".';
    }
    if (empty($data['Transaction']['Id'])) {
      $errors[] = 'data must contain valid "Transaction.Id".';
    }
    if (empty($data['Transaction']['Type'] || SaferpayClientInterface::TRANSACTION_TYPE_REFUND !== $data['Transaction']['Type'])) {
      $errors[] = 'data must contain valid "Transaction.Type".';
    }
    if (empty($data['Transaction']['Status'] || SaferpayClientInterface::TRANSACTION_AUTHORIZED !== $data['Transaction']['Status'])) {
      $errors[] = 'data must contain valid "Transaction.Status".';
    }

    if (!empty($errors)) {
      $this->getLogger('payment_saferpay')
        ->error('Payment ' . $payment->id() . ': Refund data validation failed: ' . implode(', ', $errors));
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get API URL by endpoint ID.
   *
   * @param string $endpoint_id
   *   Endpoint unique identifier.
   * @param array $config
   *   Plugin configuration.
   *
   * @return string
   *   API URL.
   */
  protected function getApiUrl(string $endpoint_id, array $config): string {
    $base_uri = rtrim($this->config->get(!empty($config['auth']['test']) ? 'api_uri_test' : 'api_uri'), '/');
    $path = ltrim($this->config->get('api_' . $endpoint_id), '/');

    return $base_uri . '/' . $path;
  }

  /**
   * Enforce session for anonymous users, required for CSRF token to work.
   *
   * @see https://www.drupal.org/project/drupal/issues/2730351
   */
  protected function ensureSession() {
    if ($this->currentUser->isAnonymous()) {
      $this->currentRequest
        ->getSession()
        ->set('forced', TRUE);
    }
  }

}
